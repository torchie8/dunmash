program DunMash;

uses Crt, sysutils;

type TAsset = record
                Name, Desc, FlavorText, UnlockCondition: string;                //all must be under 60 chars or hacked
                Count, Price, BasePrice, IncPrice, SellPrice: int64;
                Multiplier: real;
                Locked, Defined: boolean;
              end;
     TUpgrade = record
                  Name, Desc, FlavorText, UnlockCondition: string;              //all must be under 60 chars or hacked
                  Price: int64;
                  Locked, Defined: boolean;
                  Owned: integer;
     end;

     TKeypress = record
                   Main, AssetMenu, AssetWindow, AssetSell, Quit, UpgradeMenu, UpgradeWindow: char;
     end;

var dun: double;
    dps, dpc: double;
    HitCounter: double;
    HitCounterTimer: integer;
    quit: boolean;
    DebugMode: boolean;
    seenIntro: integer;
    KeyReleased: boolean;
    ConsoleInput: string;
    ErrorMessage: array[0..3] of string;
    framerate: integer;
    NumberOfAssetPages, AssetPage, AssetCursorPos: integer;
    Asset: array of array of TAsset;
    Keypress: TKeypress;
    Upgrade: array of array of TUpgrade;
    NumberOfUpgradePages, UpgradePage, UpgradeCursorPos: integer;

procedure drawBox(W, H: integer);                                               //draws textbox
var i, j: integer;
  begin
    gotoXY(((80-W) div 2), ((24-H) div 2));
    write('+');
    for i:=1 to (W-2) do write('-');                                            //draw top row
    write('+');
    for j:=1 to (H-2) do
      begin
        gotoXY(((80-W) div 2), ((24-H) div 2 + j));
        write('|');                                                             //draw sides and blank background
        for i:=1 to (W-2) do write(#00);
        write('|');
      end;
    gotoXY(((80-W) div 2), ((24-H) div 2 + H - 1));
    write('+');
    for i:=1 to (W-2) do write('-');                                            //draw bottom
    write('+');
  end;

procedure cwrite(S: string; y: integer);                                        //writes a centered string on line y
  begin
    gotoXY(((80-length(S)) div 2), y);
    write(S);
  end;

procedure drawErrorBox(i: integer);
  begin
    drawBox(length(ErrorMessage[i])+4, 4);                                      //draw error message[i]
    cwrite(ErrorMessage[i], 11);
    cwrite('Press any key...', 12);
    readln;                                                                     //wait for input
  end;

procedure enableDebugMode;
  begin
    DebugMode := true;
    drawBox(25,4);                                                              //display debug mode activation message
    cwrite('Debug mode activated!', 11);
    cwrite('Press any key...', 12);
    readln;                                                                     //wait for input
  end;

procedure showIntro;
  begin
    drawBox(74, 20);                                                            //draws helpscreen box
    gotoXY(11,3);
    write('____                _  _  _               _     ');                  //ASCII logo
    gotoXY(11,4);
    write('|   \              | \/ \/ |  ___   ___  | | __ ');
    gotoXY(11,5);
    write('|/\  \ /\ /\ |\_/\ |  _ _  | /   \ / __\ | |/  \');
    gotoXY(11,6);
    write('|\/  / ||_|| || || | | | | | |() | \__ \ |  _  |   v0.3');
    gotoXY(11,7);
    write('\___/  \___/ \/ \/ \_| | |_/ \___| \___/ \_/ \_/   by torchie');
    if seenIntro = 0 then readln;                                               //wait for keypress on first read
    cwrite('Welcome to DunMash! Take some time to remember these handy hotkeys:', 9);
    gotoXY(14,10);                                                              //writes helpscreen text
    writeln('- Press  [ESC]    to quit');
    gotoXY(14,11);
    writeln('- Press  [A]/[U]  to toggle asset/upgrade menu');
    gotoXY(14,12);
    writeln('- Mash   [SPACE]  to generate dun');
    if seenIntro = 0 then readln;                                               //wait for keypress on first read
    cwrite('Generate dun to progress. The more dun you make,', 14);
    cwrite('the more progress you become able to make.', 15);
    cwrite('Go wild!', 16);
    cwrite('You can access this screen anytime by pressing F1.', 18);
    readln;
    seenIntro := seenIntro + 1;                                                 //to be used later
    clrscr;                                                                     //clear screen before proceeding
  end;

procedure init;                                                                 //all vars and settings set to default
  begin
    TextColor(White);                                                           //sets text color to white globally
    dun := 0;                                                                   //sets starting dun to 0
    dps := 0;                                                                   //sets starting dps to 0
    dpc := 1;                                                                   //DPC shouldn't be 0 at game start
    KeyReleased := true;                                                        //enables SPACE mashing
    DebugMode := false;                                                         //sets debug mode to off
    seenIntro := 0;                                                             //sets intro views to 0
    framerate := 40;                                                            //sets default framerate
    ErrorMessage[0] := 'Error: Illegal command!';                               //set error messages
    ErrorMessage[1] := 'Error: Debug mode required!';
    ErrorMessage[2] := 'You don''t have any left!';
    ErrorMessage[3] := 'You don''t have enough dun!';
    NumberOfAssetPages := 1;
    NumberOfUpgradePages := 1;
    SetLength(Asset,NumberOfAssetPages,6);                                      //init dynamic arrays (DO NOT TOUCH)
    SetLength(Upgrade,NumberOfUpgradePages,6);
  end;

procedure initAssets;                                                           //sets default values for all asset related vars
var i,j: integer;
  begin
    Asset[0,0].Name := 'Durable Spork';                                         //stats for Durable Spork
    Asset[0,0].Desc := 'Gives your dun-per-mash a modest boost.';
    Asset[0,0].FlavorText := '"Scrape more dun and scoop it up!"';
    Asset[0,0].BasePrice := 100;
    Asset[0,0].IncPrice := 25;
    Asset[0,0].Locked := false;
    Asset[0,0].UnlockCondition := 'If you''re seeing this, there''s an error on the loose! Run!';

    Asset[0,1].Name := 'Modular Mashmachine';                                   //stats for Modular Mashmachine
    Asset[0,1].Desc := 'Has a strong robo-finger that helps you with the mashing.';
    Asset[0,1].FlavorText := '"More mashery, now automatized and futureproof."';
    Asset[0,1].BasePrice := 200;
    Asset[0,1].IncPrice := 40;
    Asset[0,1].Locked := false;
    Asset[0,1].UnlockCondition := 'If you''re seeing this, there''s an error on the loose! Run!';

    Asset[0,2].Name := 'Dun-gen';                                               //stats for Dun-gen
    Asset[0,2].Desc := 'Does what it says on the tin. At 5 dun per second.';
    Asset[0,2].FlavorText := '"NOT Dun-geon. That one''s a place you never want to see."';
    Asset[0,2].BasePrice := 1500;
    Asset[0,2].IncPrice := 200;
    Asset[0,2].Locked := true;
    Asset[0,2].UnlockCondition := 'Buy enough Mashmachines to replace one of your hands.';

    AssetPage := 0;                                                             //sets asset menu to start on page 1
    AssetCursorPos := 0;                                                        //sets asset menu cursor to start on entry 1
    for i:=0 to NumberOfAssetPages-1 do
      begin
        for j:=0 to 5 do Asset[i,j].Count := 0;                                 //sets all asset counts to 0 no matter how many exist
        for j:=0 to 5 do Asset[i,j].Multiplier := 1;                            //same but with multipliers
      end;
    for i:=0 to 2 do
      Asset[0,i].Defined := true;                                               //sets first 3 assets as defined
    for i:=3 to 5 do
      Asset[0,i].Defined := false;                                              //sets assets 4-6 on page 1 as undefined
    for i:=1 to NumberOfAssetPages-1 do
      for j:=0 to 5 do Asset[i,j].Defined := false;                             //sets all other assets as undefined
  end;

procedure initUpgrades;
var i,j: integer;
  begin
    Upgrade[0,0].Name := 'Plast-O-Metal-O-Matic';                               //Plast-O-Metal-O-Matic stats
    Upgrade[0,0].Desc := 'Makes metal sporks that give twice the dun.';
    Upgrade[0,0].FlavorText := '"Who woulda thunk it?"';
    Upgrade[0,0].Price := 500;
    Upgrade[0,0].Locked := false;
    Upgrade[0,0].UnlockCondition := 'If you''re seeing this, there''s an error on the loose! Run!';

    Upgrade[0,1].Name := 'Speed Fix';
    Upgrade[0,1].Desc := 'Fixes framerate for mashmachines. Not actually a narcotic.';
    Upgrade[0,1].FlavorText := '"Everybody gets one."';
    Upgrade[0,1].Price := 1300;
    Upgrade[0,1].Locked := false;
    Upgrade[0,1].UnlockCondition := 'If you''re seeing this, there''s an error on the loose! Run!';

    Upgrade[0,2].Name := 'Pascal''s Law 101';
    Upgrade[0,2].Desc := 'Teaches your mashmachines to push it really hard.';
    Upgrade[0,2].FlavorText := '"Apply more pressure on the dev- I mean, the keyboard."';
    Upgrade[0,2].Price := 4000;
    Upgrade[0,2].Locked := true;
    Upgrade[0,2].UnlockCondition := 'Press [ALT]+[C] sometime and gowild.';

    UpgradePage := 0;
    UpgradeCursorPos := 0;
    for i:=0 to NumberOfUpgradePages-1 do
      for j:=0 to 5 do
        begin
          Upgrade[i,j].Owned := 0;
          Upgrade[i,j].Defined := false;
        end;
    Upgrade[0,0].Defined := true;
  end;

procedure updateAssetPrice;
var i,j: integer;
  begin
    for i:=0 to NumberOfAssetPages-1 do                                         //executes for all pages
      for j:=0 to 5 do                                                          //executes for all entries on page
        begin
          Asset[i,j].Price := Asset[i,j].BasePrice + Asset[i,j].IncPrice * Asset[i,j].Count;                     //sets price to base + total increase
          Asset[i,j].SellPrice := (Asset[i,j].BasePrice + Asset[i,j].IncPrice * (Asset[i,j].Count - 1)) div 2;   //sets sell price to half of last buy price
        end;
  end;

procedure updateAssetAvailability;
  begin
    if Asset[0,1].Count >= 5 then Asset[0,2].Locked := false;                   //Dun-gen unlocked if player has owned 5 Modular Mashmachines
  end;

procedure updateUpgradeAvailability;
  begin
    if Asset[0,1].Count >= 5 then Upgrade[0,1].Defined := true;                 //Speed Fix defined if player has owned 5 Modular Mashmachines
    if Asset[0,1].Count >= 10 then Upgrade[0,2].Defined := true;                //Pascal's Law 101 defined if player has owned 10 Modular Mashmachines
  end;

procedure updateUpgradeEffects;
  begin
    Asset[0,0].Multiplier := 1 + Upgrade[0,0].Owned;                            //Durable Spork mult modded by Plast-O-Metal-O-Matic
    Asset[0,1].Multiplier := 1 + Upgrade[0,1].Owned + 1.5*Upgrade[0,2].Owned;   //Modular Mashmachines mult modded by Speed Fix and Pascal's Law 101
  end;

procedure showdun;
  begin                                                                         //woo, clrscr-less dun display routine! smooth af!
    gotoXY(1,1);                                                                //WARN: need to clrscr after every other visible proc!
    write(' ==============================================================================');
    gotoXY(1,2);
    write('               You currently possess: ', dun:13:1, ' dun.');              //displays current dun bank
    gotoXY(1,3);
    write('               You currently generate: ', dps:12:1, ' dun per second.');  //displays current dps
    gotoXY(1,4);
    write(' ==============================================================================');
  end;

procedure updateHitCounter;                                                          //takes care of the timer and display, not value
  begin
    if (Keypress.Main <> (#00)) AND (Keypress.Main <> (#32)) then                                        //resets counter when non-SPACE keys pressed
      begin
        HitCounterTimer := 0;
        HitCounter := 0;
      end;
    if HitCounterTimer > 0 then                                                      //executes if hit counter is on
      begin
        HitCounterTimer := HitCounterTimer - 1;                                      //reduce timer by 1 (this is looped every frame)
        gotoXY(63,2);
        TextColor(Yellow);
        write('+', HitCounter:8:1);                                                  //display counter in color yellow
        TextColor(White);
      end
    else                                                                             //executes if hit counter is off
      begin
        HitCounter := 0;                                                             //reset to 0 just in case
        gotoXY(63,2);
        write('         ');                                                          //remove counter text
      end;
  end;

procedure updatedpc;
  begin
    dpc := 1 + 0.25*Asset[0,0].Count*Asset[0,0].Multiplier;                           //adds Durable Sporks to dpc total
  end;

procedure updatedps;
  begin
    dps := Asset[0,1].Count*Asset[0,1].Multiplier                                    //adds Modular Mashmachines to dps total
          +Asset[0,2].Count*Asset[0,2].Multiplier*5;                                 //adds Dun-gen to dps total
  end;

procedure QuitDialog;
  begin
    Keypress.Quit := (#00);                                                          //resets keypress var
    drawBox(66, 4);
    cwrite('There is still dun to be made! Are you sure you want to quit?', 11);     //display confirmation message
    cwrite('(Y/N)', 12);
    while (Keypress.Quit <> #110) and (Keypress.Quit <> #121) do                     //ignores other keys than Y/N
      Keypress.Quit := ReadKey;                                                      //reads keypress
      if Keypress.Quit = (#121) then                                                 //sets program to quit when Y pressed
        quit := true;
    clrscr;                                                                          //clears screen (useful if not set to quit)
  end;

procedure openConsole;
  begin
    drawBox(70,20);                                                                  //draw console box
    gotoXY(8,4);
    cursoron;                                                                        //set cursor to correct spot and make it visible
    readln(ConsoleInput);                                                            //read input
    cursoroff;                                                                       //hide cursor again
      case ConsoleInput of
        ('bugsgetyegone'): enableDebugMode;                                          //enable console cheats
        ('gowild'): if Upgrade[0,2].Defined = true then Upgrade[0,2].Locked := false;//unlocks Pascal's Law 101
        else drawErrorBox(0);                                                        //display illegal command error when invalid command entered
      end;
    clrscr;                                                                          //clear screen before exiting
  end;

procedure drawUpgrades;
begin                                                                                //long but functional upgrade list-drawing routine
  updateUpgradeAvailability;                                                         //first, check for newly available upgrades
    if Upgrade[UpgradePage,0].Defined = true then
      begin
        gotoXY(15,7);
        write(Upgrade[UpgradePage,0].Name);
        gotoXY(15,8);
        write('Price: ', Upgrade[UpgradePage,0].Price);
        gotoXY(15,9);
        write('Owned: ');
        if Upgrade[UpgradePage,0].Owned = 1 then
          begin
            TextColor(10);
            write('Yes');
            TextColor(white);
          end;
        if Upgrade[UpgradePage,0].Owned = 0 then
          begin
            TextColor(12);
            write('No');
            TextColor(white);
            gotoXY(15,10);
            if Upgrade[UpgradePage,0].Locked = false then
              begin
                TextColor(10);
                write('Available');
                TextColor(white);
              end;
            if Upgrade[UpgradePage,0].Locked = true then
              begin
                TextColor(12);
                write('Locked');
                TextColor(white);
            end;
          end;
      end;
    if Upgrade[UpgradePage,1].Defined = true then
      begin
        gotoXY(15,12);
        write(Upgrade[UpgradePage,1].Name);
        gotoXY(15,13);
        write('Price: ', Upgrade[UpgradePage,1].Price);
        gotoXY(15,14);
        write('Owned: ');
        if Upgrade[UpgradePage,1].Owned = 1 then
          begin
            TextColor(10);
            write('Yes');
            TextColor(white);
          end;
        if Upgrade[UpgradePage,1].Owned = 0 then
          begin
            TextColor(12);
            write('No');
            TextColor(white);
            gotoXY(15,15);
            if Upgrade[UpgradePage,1].Locked = false then
              begin
                TextColor(10);
                write('Available');
                TextColor(white);
              end;
            if Upgrade[UpgradePage,1].Locked = true then
              begin
                TextColor(12);
                write('Locked');
                TextColor(white);
            end;
          end;
      end;
    if Upgrade[UpgradePage,2].Defined = true then
      begin
        gotoXY(15,17);
        write(Upgrade[UpgradePage,2].Name);
        gotoXY(15,18);
        write('Price: ', Upgrade[UpgradePage,2].Price);
        gotoXY(15,19);
        write('Owned: ');
        if Upgrade[UpgradePage,2].Owned = 1 then
          begin
            TextColor(10);
            write('Yes');
            TextColor(white);
          end;
        if Upgrade[UpgradePage,2].Owned = 0 then
          begin
            TextColor(12);
            write('No');
            TextColor(white);
            gotoXY(15,20);
            if Upgrade[UpgradePage,2].Locked = false then
              begin
                TextColor(10);
                write('Available');
                TextColor(white);
              end;
            if Upgrade[UpgradePage,2].Locked = true then
              begin
                TextColor(12);
                write('Locked');
                TextColor(white);
            end;
          end;
      end;
    if Upgrade[UpgradePage,3].Defined = true then
      begin
        gotoXY(53,7);
        write(Upgrade[UpgradePage,3].Name);
        gotoXY(53,8);
        write('Price: ', Upgrade[UpgradePage,3].Price);
        gotoXY(53,9);
        write('Owned: ');
        if Upgrade[UpgradePage,3].Owned = 1 then
          begin
            TextColor(10);
            write('Yes');
            TextColor(white);
          end;
        if Upgrade[UpgradePage,3].Owned = 0 then
          begin
            TextColor(12);
            write('No');
            TextColor(white);
            gotoXY(53,10);
            if Upgrade[UpgradePage,3].Locked = false then
              begin
                TextColor(10);
                write('Available');
                TextColor(white);
              end;
            if Upgrade[UpgradePage,3].Locked = true then
              begin
                TextColor(12);
                write('Locked');
                TextColor(white);
            end;
          end;
      end;
    if Upgrade[UpgradePage,4].Defined = true then
      begin
        gotoXY(53,12);
        write(Upgrade[UpgradePage,4].Name);
        gotoXY(53,13);
        write('Price: ', Upgrade[UpgradePage,4].Price);
        gotoXY(53,14);
        write('Owned: ');
        if Upgrade[UpgradePage,4].Owned = 1 then
          begin
            TextColor(10);
            write('Yes');
            TextColor(white);
          end;
        if Upgrade[UpgradePage,4].Owned = 0 then
          begin
            TextColor(12);
            write('No');
            TextColor(white);
            gotoXY(53,15);
            if Upgrade[UpgradePage,4].Locked = false then
              begin
                TextColor(10);
                write('Available');
                TextColor(white);
              end;
            if Upgrade[UpgradePage,4].Locked = true then
              begin
                TextColor(12);
                write('Locked');
                TextColor(white);
            end;
          end;
      end;
    if Upgrade[UpgradePage,5].Defined = true then
      begin
        gotoXY(53,17);
        write(Upgrade[UpgradePage,5].Name);
        gotoXY(53,18);
        write('Price: ', Upgrade[UpgradePage,5].Price);
        gotoXY(53,19);
        write('Owned: ');
        if Upgrade[UpgradePage,5].Owned = 1 then
          begin
            TextColor(10);
            write('Yes');
            TextColor(white);
          end;
        if Upgrade[UpgradePage,5].Owned = 0 then
          begin
            TextColor(12);
            write('No');
            TextColor(white);
            gotoXY(53,20);
            if Upgrade[UpgradePage,5].Locked = false then
              begin
                TextColor(10);
                write('Available');
                TextColor(white);
              end;
            if Upgrade[UpgradePage,5].Locked = true then
              begin
                TextColor(12);
                write('Locked');
                TextColor(white);
            end;
          end;
      end;
  end;

procedure setUpgradeCursor;
  begin
    if Upgrade[UpgradePage,UpgradeCursorPos].Defined = true then                     //check to prevent selecting invalid upgrades
      begin
        case UpgradeCursorPos of                                                     //clumsy but fast cursor drawing routine
        0: begin gotoXY(12,8);                                                       //draws cursor at position 2
                 write('*');
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        1: begin gotoXY(12,8);                                                       //draws cursor at position 2
                 write(#00);
                 gotoXY(12,13);
                 write('*');
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        2: begin gotoXY(12,8);                                                       //draws cursor at position 3
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write('*');
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        3: begin gotoXY(12,8);                                                       //draws cursor at position 4
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write('*');
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        4: begin gotoXY(12,8);                                                       //draws cursor at position 5
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write('*');
                 gotoXY(50,18);
                 write(#00);
           end;
        5: begin gotoXY(12,8);                                                       //draws cursor at position 6
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write('*');
           end;
        end;
      end;
  end;

procedure drawUpgradeWindow;
  begin
    drawBox(60,9);                                                                   //this block draws the static window layout
    cwrite('+----------------------------------------------------------+',9);
    if Upgrade[UpgradePage,UpgradeCursorPos].Owned = 0 then cwrite('[B] to buy', 14)
    else cwrite('You already have this upgrade', 14);
    repeat                                                                           //loop that redraws variables and then scans keypress
      updateUpgradeAvailability;                                                     //updates upgrade locked/defined status
      Keypress.UpgradeWindow := (#00);                                               //resets keypress var
      cwrite(Upgrade[UpgradePage,UpgradeCursorPos].Name,8);                          //displays upgrade name
      cwrite(Upgrade[UpgradePage,UpgradeCursorPos].Desc,11);                         //displays upgrade description
      cwrite(Upgrade[UpgradePage,UpgradeCursorPos].FlavorText,12);                   //displays upgrade flavor text
      gotoXY(33,10);
      if Upgrade[UpgradePage,UpgradeCursorPos].Price < 1000000 then write('Price: ', Upgrade[UpgradePage,UpgradeCursorPos].Price)
      else if Upgrade[UpgradePage,UpgradeCursorPos].Price < 1000000000 then write ('Price: ', Upgrade[UpgradePage,UpgradeCursorPos].Price, ' million');
      gotoXY(29,13);
      write('Bank: ', trunc(dun):11, ' dun');                                        //displays current dun bank
      Keypress.UpgradeWindow := ReadKey;                                             //scans keypress
      case Keypress.UpgradeWindow of                                                 //begin of buy/sell block
        (#98): if Upgrade[UpgradePage,UpgradeCursorPos].Owned = 0 then begin         {B}
                   if dun >= Upgrade[UpgradePage,UpgradeCursorPos].Price then        //checks if enough dun
                   begin
                     dun := dun - Upgrade[UpgradePage,UpgradeCursorPos].Price;       //price is removed from dun bank
                     Upgrade[UpgradePage,UpgradeCursorPos].Owned := 1;               //upgrade is added
                   end
                   else                                 //executes if not enough dun
                   begin
                     drawErrorBox(3);                   //insufficient funds error
                     drawBox(60,9);                     //cleanup block to resume upgrade window functions
                     cwrite('+----------------------------------------------------------+',9);
                     if Upgrade[UpgradePage,UpgradeCursorPos].Owned = 0 then cwrite('[B] to buy', 14)
                     else cwrite('You already have this upgrade', 14);
                   end;
               end;                                     //end of buy block
      end;
    until Keypress.UpgradeWindow = (#27);
    clrscr;                                             //cleanup block to resume upgrade menu functions
    drawBox(80,23);
    cwrite('UPGRADES',3);
    gotoXY(35,4);
    write('(page ', UpgradePage+1, '/', NumberOfUpgradePages, ')');
    cwrite('[PgDn]/[PgUp] to scroll pages', 22);
  end;

procedure UpgradeMenu;
  begin
    clrscr;                                             //this block sets up layout and text
    drawBox(80,24);
    cwrite('UPGRADES',3);
    gotoXY(35,4);
    write('(page ', UpgradePage+1, '/', NumberOfUpgradePages, ')');
    cwrite('[PgDn]/[PgUp] to scroll pages', 22);
    repeat
      Keypress.UpgradeMenu := (#00);                    //reset keypress
      drawUpgrades;                                     //list assets onto page
      setUpgradeCursor;                                 //draw cursor
      Keypress.UpgradeMenu := ReadKey;                  //scan keypress
        case Keypress.UpgradeMenu of
          (#75): if (UpgradeCursorPos >= 3) AND (UpgradeCursorPos <= 5)
                   then if Upgrade[UpgradePage,(UpgradeCursorPos-3)].Defined = true
                     then UpgradeCursorPos := UpgradeCursorPos - 3;
          (#77): if (UpgradeCursorPos >= 0) AND (UpgradeCursorPos <= 2)
                   then if Upgrade[UpgradePage,(UpgradeCursorPos+3)].Defined = true
                     then UpgradeCursorPos := UpgradeCursorPos + 3;               //this block does movement via arrow keys
          (#72): if (UpgradeCursorPos <> 0) AND (UpgradeCursorPos <> 3)           //always checks if upgrade is defined and stops if not
                   then if Upgrade[UpgradePage,(UpgradeCursorPos-1)].Defined = true
                     then UpgradeCursorPos := UpgradeCursorPos - 1;
          (#80): if (UpgradeCursorPos <> 2) AND (UpgradeCursorPos <> 5)
                   then if Upgrade[UpgradePage,(UpgradeCursorPos+1)].Defined = true
                     then UpgradeCursorPos := UpgradeCursorPos + 1;
          (#81): if UpgradePage < NumberOfUpgradePages-1 then
                 begin UpgradePage := UpgradePage + 1;
                       clrscr;
                       drawBox(80,23);                  //this block redraws page when scrolling down
                       cwrite('UPGRADES',3);
                       gotoXY(35,4);
                       write('(page ', UpgradePage+1, '/', NumberOfUpgradePages, ')');
                       cwrite('[PgDn]/[PgUp] to scroll pages', 22);
                 end;
          (#73): if UpgradePage > 0 then                  {PgUp}
                 begin UpgradePage := UpgradePage - 1;
                       clrscr;
                       drawBox(80,23);                  //this block redraws page when scrolling up
                       cwrite('UPGRADES',3);
                       gotoXY(35,4);
                       write('(page ', UpgradePage+1, '/', NumberOfUpgradePages, ')');
                       cwrite('[PgDn]/[PgUp] to scroll pages', 22);
                 end;
          (#13): if Upgrade[UpgradePage,UpgradeCursorPos].Defined then                 //stops opening undefined upgrades (shouldn't happen though)
                   if not Upgrade[UpgradePage,UpgradeCursorPos].Locked then drawUpgradeWindow  //stops opening locked upgrades
                   else begin
                          drawBox(60,6);                                               //shows the "item locked" textbox
                          cwrite('Wait up! That item is locked!', 10);
                          cwrite('Here''s how you unlock it:', 11);
                          cwrite(Upgrade[UpgradePage,UpgradeCursorPos].UnlockCondition, 12); //writes instruction on how to unlock
                          cwrite('Press any key if you''ve got it!', 13);
                          readln;
                          clrscr;                                             //cleanup block to resume asset menu functions
                          drawBox(80,23);
                          cwrite('UPGRADES',3);
                          gotoXY(35,4);
                          write('(page ', UpgradePage+1, '/', NumberOfUpgradePages, ')');
                          cwrite('[PgDn]/[PgUp] to scroll pages', 22);
                        end;
        end;
    until Keypress.UpgradeMenu = (#27);
    clrscr;
  end;

procedure drawAssets;
begin                                                                                //long but functional asset list-drawing routine
  updateAssetAvailability;                                                           //first, check for newly available assets
    if Asset[AssetPage,0].Defined = true then
      begin
        gotoXY(15,7);
        write(Asset[AssetPage,0].Name);                                              //display asset name
        gotoXY(15,8);
        write('Price: ', Asset[AssetPage,0].Price);                                  //display asset price
        gotoXY(15,9);
        write('Owned: ', Asset[AssetPage,0].Count);                                  //display asset count
        gotoXY(15,10);
        if Asset[AssetPage,0].Locked = false then                                    //if asset unlocked then...
          begin
            TextColor(10);
            write('Available');                                                      //...display available in color green
            TextColor(white);
          end;
        if Asset[AssetPage,0].Locked = true then                                     //if asset locked then...
          begin
            TextColor(12);
            write('Locked');                                                         //...display locked in color red
            TextColor(white);                                                        //this is repeated 6 times for all 6 entries
          end;
      end;
    if Asset[AssetPage,1].Defined = true then
      begin
        gotoXY(15,12);
        write(Asset[AssetPage,1].Name);
        gotoXY(15,13);
        write('Price: ', Asset[AssetPage,1].Price);
        gotoXY(15,14);
        write('Owned: ', Asset[AssetPage,1].Count);
        gotoXY(15,15);
        if Asset[AssetPage,1].Locked = false then
          begin
            TextColor(10);
            write('Available');
            TextColor(white);
          end;
        if Asset[AssetPage,1].Locked = true then
          begin
            TextColor(12);
            write('Locked');
            TextColor(white);
          end;
      end;
    if Asset[AssetPage,2].Defined = true then
      begin
        gotoXY(15,17);
        write(Asset[AssetPage,2].Name);
        gotoXY(15,18);
        write('Price: ', Asset[AssetPage,2].Price);
        gotoXY(15,19);
        write('Owned: ', Asset[AssetPage,2].Count);
        gotoXY(15,20);
        if Asset[AssetPage,2].Locked = false then
          begin
            TextColor(10);
            write('Available');
            TextColor(white);
          end;
        if Asset[AssetPage,2].Locked = true then
          begin
            TextColor(12);
            write('Locked');
            TextColor(white);
          end;
      end;
    if Asset[AssetPage,3].Defined = true then
      begin
        gotoXY(53,7);
        write(Asset[AssetPage,3].Name);
        gotoXY(53,8);
        write('Price: ', Asset[AssetPage,3].Price);
        gotoXY(53,9);
        write('Owned: ', Asset[AssetPage,3].Count);
        gotoXY(53,10);
        if Asset[AssetPage,3].Locked = false then
          begin
            TextColor(10);
            write('Available');
            TextColor(white);
          end;
        if Asset[AssetPage,3].Locked = true then
          begin
            TextColor(12);
            write('Locked');
            TextColor(white);
          end;
      end;
    if Asset[AssetPage,4].Defined = true then
      begin
        gotoXY(53,12);
        write(Asset[AssetPage,4].Name);
        gotoXY(53,13);
        write('Price: ', Asset[AssetPage,4].Price);
        gotoXY(53,14);
        write('Owned: ', Asset[AssetPage,4].Count);
        gotoXY(53,15);
        if Asset[AssetPage,4].Locked = false then
          begin
            TextColor(10);
            write('Available');
            TextColor(white);
          end;
        if Asset[AssetPage,4].Locked = true then
          begin
            TextColor(12);
            write('Locked');
            TextColor(white);
          end;
      end;
    if Asset[AssetPage,5].Defined = true then
      begin
        gotoXY(53,17);
        write(Asset[AssetPage,5].Name);
        gotoXY(53,18);
        write('Price: ', Asset[AssetPage,5].Price);
        gotoXY(53,19);
        write('Owned: ', Asset[AssetPage,5].Count);
        gotoXY(53,20);
        if Asset[AssetPage,5].Locked = false then
          begin
            TextColor(10);
            write('Available');
            TextColor(white);
          end;
        if Asset[AssetPage,5].Locked = true then
          begin
            TextColor(12);
            write('Locked');
            TextColor(white);
          end;
      end;
  end;

procedure setAssetCursor;
  begin
    if Asset[AssetPage,AssetCursorPos].Defined = true then                           //another check to prevent selecting invalid assets
      begin
        case AssetCursorPos of                                                       //clumsy but fast cursor drawing routine
        0: begin gotoXY(12,8);                                                       //draws cursor at position 2
                 write('*');
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        1: begin gotoXY(12,8);                                                       //draws cursor at position 2
                 write(#00);
                 gotoXY(12,13);
                 write('*');
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        2: begin gotoXY(12,8);                                                       //draws cursor at position 3
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write('*');
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        3: begin gotoXY(12,8);                                                       //draws cursor at position 4
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write('*');
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write(#00);
           end;
        4: begin gotoXY(12,8);                                                       //draws cursor at position 5
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write('*');
                 gotoXY(50,18);
                 write(#00);
           end;
        5: begin gotoXY(12,8);                                                       //draws cursor at position 6
                 write(#00);
                 gotoXY(12,13);
                 write(#00);
                 gotoXY(12,18);
                 write(#00);
                 gotoXY(50,8);
                 write(#00);
                 gotoXY(50,13);
                 write(#00);
                 gotoXY(50,18);
                 write('*');
           end;
        end;
      end;
  end;

procedure drawAssetWindow;
  begin
    drawBox(60,9);                                                                   //this block draws the static window layout
    cwrite('+----------------------------------------------------------+',9);
    cwrite('[B] to buy, [S] to sell', 14);
    repeat                                                                           //loop that redraws variables and then scans keypress
      updateAssetPrice;                                                              //updates asset prices
      updateAssetAvailability;                                                       //updates asset locked/defined status
      Keypress.AssetWindow := (#00);                                                 //resets keypress var
      cwrite(Asset[AssetPage,AssetCursorPos].Name,8);                                //displays asset name
      cwrite(Asset[AssetPage,AssetCursorPos].Desc,11);                               //displays asset description
      cwrite(Asset[AssetPage,AssetCursorPos].FlavorText,12);                         //displays asset flavor text
      gotoXY(25,10);
      if Asset[AssetPage,AssetCursorPos].Price < 1000000 then write('Price: ', Asset[AssetPage,AssetCursorPos].Price)
      else if Asset[AssetPage,AssetCursorPos].Price < 1000000000 then write ('Price: ', Asset[AssetPage,AssetCursorPos].Price, ' million');
      gotoXY(45,10);
      write('Owned: ', Asset[AssetPage,AssetCursorPos].Count);                       //displays number of asset owned
      gotoXY(29,13);
      write('Bank: ', trunc(dun):11, ' dun');                                        //displays current dun bank
      Keypress.AssetWindow := ReadKey;                                               //scans keypress
      case Keypress.AssetWindow of                                                   //begin of buy/sell block
        (#98): begin                                                                 {B}
                   if dun >= Asset[AssetPage,AssetCursorPos].Price then              //checks if enough dun
                   begin
                     dun := dun - Asset[AssetPage,AssetCursorPos].Price;             //price is removed from dun bank
                     Asset[AssetPage,AssetCursorPos].Count := Asset[AssetPage,AssetCursorPos].Count + 1;               //asset is added
                   end
                   else                                 //executes if not enough dun
                   begin
                     drawErrorBox(3);                   //insufficient funds error
                     drawBox(60,9);                     //cleanup block to resume asset window functions
                     cwrite('+----------------------------------------------------------+',9);
                     cwrite('[B] to buy, [S] to sell', 14);
                   end;
               end;                                     //end of buy block
        (#115): begin                                   {S}
                if Asset[AssetPage,AssetCursorPos].Count > 0 then                    //checks if assets are owned so they can be sold
                begin
                  drawBox(44,7);                                                     //this block draws the sell window (buy doesn't have one)
                  cwrite('You are about to sell one of this asset:', 9);
                  cwrite(Asset[AssetPage,AssetCursorPos].Name, 10);                  //displays name of asset to be sold
                  gotoXY(35,11);
                  write('for ', (Asset[AssetPage,AssetCursorPos].SellPrice), ' dun.');   //displays current sell price for asset (updated earlier)
                  cwrite('Are you sure?', 12);
                  cwrite('(Y/N)', 13);
                  while (Keypress.AssetSell <> (#110)) AND (Keypress.AssetSell <> (#121)) do  //loops each keypress until Y or N pressed
                    begin
                      Keypress.AssetSell := (#00);                                            //resets key var
                      Keypress.AssetSell := ReadKey;                                          //reads keypress
                      if Keypress.AssetSell = (#121) then                                     //executes if Y pressed
                        begin
                          if Asset[AssetPage,AssetCursorPos].Count > 0 then                   //checks again if asset owned > 0 just in case
                            begin
                              Asset[AssetPage,AssetCursorPos].Count := Asset[AssetPage,AssetCursorPos].Count - 1;    //decreases asset count
                              dun := dun + Asset[AssetPage,AssetCursorPos].SellPrice;          //adds sell price to dun bank
                            end
                          else drawErrorBox(2);                                      //displays none left error if asset count = 0 (secondary check)
                        end;
                    end;                                                             //end of keypress scan loop
                  Keypress.AssetSell := (#00);                                       //resets key var (necessary for selling more in a row)
                end
                else drawErrorBox(2);                   //displays none left error if asset count = 0
                drawBox(60,9);                          //cleanup block to resume asset window functions
                cwrite('+----------------------------------------------------------+',9);
                cwrite('[B] to buy, [S] to sell', 14);
                end;
      end;                                              //end of buy/sell block
    until Keypress.AssetWindow = (#27);
    clrscr;                                             //cleanup block to resume asset menu functions
    drawBox(80,23);
    cwrite('ASSETS',3);
    gotoXY(35,4);
    write('(page ', AssetPage+1, '/', NumberOfAssetPages, ')');
    cwrite('[PgDn]/[PgUp] to scroll pages', 22);
  end;

procedure AssetMenu;
  begin
    clrscr;                                             //this block sets up layout and text
    drawBox(80,24);
    cwrite('ASSETS',3);
    gotoXY(35,4);
    write('(page ', AssetPage+1, '/', NumberOfAssetPages, ')');
    cwrite('[PgDn]/[PgUp] to scroll pages', 22);
    repeat
      Keypress.AssetMenu := (#00);                      //reset keypress
      updateAssetPrice;                                 //updates prices
      drawAssets;                                       //list assets onto page
      setAssetCursor;                                   //draw cursor
      Keypress.AssetMenu := ReadKey;                    //scan keypress
        case Keypress.AssetMenu of
          (#75): if (AssetCursorPos >= 3) AND (AssetCursorPos <= 5)
                   then if Asset[AssetPage,(AssetCursorPos-3)].Defined = true
                     then AssetCursorPos := AssetCursorPos - 3;
          (#77): if (AssetCursorPos >= 0) AND (AssetCursorPos <= 2)
                   then if Asset[AssetPage,(AssetCursorPos+3)].Defined = true
                     then AssetCursorPos := AssetCursorPos + 3;                   //this block does movement via arrow keys
          (#72): if (AssetCursorPos <> 0) AND (AssetCursorPos <> 3)               //always checks if asset is defined and stops if not
                   then if Asset[AssetPage,(AssetCursorPos-1)].Defined = true
                     then AssetCursorPos := AssetCursorPos - 1;
          (#80): if (AssetCursorPos <> 2) AND (AssetCursorPos <> 5)
                   then if Asset[AssetPage,(AssetCursorPos+1)].Defined = true
                     then AssetCursorPos := AssetCursorPos + 1;
          (#81): if AssetPage < NumberOfAssetPages-1 then
                 begin AssetPage := AssetPage + 1;
                       clrscr;
                       drawBox(80,23);                  //this block redraws page when scrolling down
                       cwrite('ASSETS',3);
                       gotoXY(35,4);
                       write('(page ', AssetPage+1, '/', NumberOfAssetPages, ')');
                       cwrite('[PgDn]/[PgUp] to scroll pages', 22);
                 end;
          (#73): if AssetPage > 0 then                  {PgUp}
                 begin AssetPage := AssetPage - 1;
                       clrscr;
                       drawBox(80,23);                  //this block redraws page when scrolling up
                       cwrite('ASSETS',3);
                       gotoXY(35,4);
                       write('(page ', AssetPage+1, '/', NumberOfAssetPages, ')');
                       cwrite('[PgDn]/[PgUp] to scroll pages', 22);
                 end;
          (#13): if Asset[AssetPage,AssetCursorPos].Defined then                       //stops opening undefined assets (shouldn't happen though)
                   if not Asset[AssetPage,AssetCursorPos].Locked then drawAssetWindow  //stops opening locked assets
                   else begin
                          drawBox(60,6);                                               //shows the "item locked" textbox
                          cwrite('Wait up! That item is locked!', 10);
                          cwrite('Here''s how you unlock it:', 11);
                          cwrite(Asset[AssetPage,AssetCursorPos].UnlockCondition, 12); //writes instruction on how to unlock
                          cwrite('Press any key if you''ve got it!', 13);
                          readln;
                          clrscr;                                             //cleanup block to resume asset menu functions
                          drawBox(80,23);
                          cwrite('ASSETS',3);
                          gotoXY(35,4);
                          write('(page ', AssetPage+1, '/', NumberOfAssetPages, ')');
                          cwrite('[PgDn]/[PgUp] to scroll pages', 22);
                        end;
        end;
    until Keypress.AssetMenu = (#27);
    clrscr;
  end;

procedure checkKeyPress;
  begin
    while KeyPressed do Keypress.Main := ReadKey;       //scan for keypresses without waiting
    case Keypress.Main of
      (#27): QuitDialog;                                {ESC}
      (#32): if KeyReleased = true then                 {SPACE}
               begin
                 KeyReleased := false;                  //check if keys aren't being held - part 2
                 dun := dun + dpc;                      //dun increment from spacemash
                 HitCounter := HitCounter + dpc;        //increases hit counter by current dpc
                 HitCounterTimer := framerate;          //sets timer to 2 seconds
               end;
      (#59): showIntro;                                 {F1}
      (#46): openConsole;                               {ALT+C}
      (#43): if DebugMode = true then dps := dps + 1;   {+}
      (#45): if DebugMode = true then dps := dps - 1;   {-}
      (#97): AssetMenu;                                 {A}
      (#117): UpgradeMenu;                              {U}
    end;
  end;

begin
  cursoroff;                                            //turn off cmdline cursor
  init;                                                 //all vars set to default
  initAssets;                                           //all asset related vars set to default
  initUpgrades;                                         //all upgrade related vars set to default
  updateAssetPrice;                                     //sets initial asset prices
  showIntro;                                            //displays F1 text
  repeat                                                //beginning of main loop
    Keypress.Main := (#00);                             //resets selection
    updateUpgradeEffects;                               //updates all effects upgrades have
    updatedps;                                          //calculates total current dps
    dun := (dun + (dps/framerate));                     //update dun count according to dps
    updatedpc;                                          //calculates total current dpc
    showdun;                                            //displays dun/dps counts
    if not KeyPressed then KeyReleased := true;         //check if keys aren't being held for dun gen.
    checkKeyPress;                                      //scans for keypresses
    updateHitCounter;                                   //displays hit counter if conditions met
    delay(1000 div framerate);                          //delays to set game framerate
  until quit = true;                                    //quit condition
end.
