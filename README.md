# DunMash

### The dunniest, mashiest game this side of the Milky Way

**Author:** Matt Black  
**Date:** 09/17/2016

DunMash is the result of my first ever foray into game development. It's a Windows-only console-based clone of Cookie Clicker written in FreePascal using the Lazarus IDE.  
The objective of the game, for those unfamiliar with the concept, is to gather as much "dun" as possible by repeatedly mashing the space key and purchasing items with this dun to accelerate the production.  
For help and hints related to the game itself, see the ingame help screen.

**Disclaimer: This project is obsolete.** I don't do Pascal anymore, and the source is ugly enough for me to deem it unmaintainable. It contains lots of inefficient, badly conceived or outright incorrect code and questionable programming practices. If you are new to programming, please do not use this code as a reference.